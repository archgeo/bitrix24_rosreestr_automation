<?php

require __DIR__ . '/vendor/autoload.php';
require __DIR__ . '/helpers.php';

use Dotenv\Dotenv;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;

$dotenv = Dotenv::createImmutable(__DIR__);
$dotenv->load();

$reestrApiToken = $_ENV['REESTR_API_TOKEN'];
$bitrix24WebhookUrl = $_ENV['BITRIX_24_WEBHOOK_URL'];
$bitrix24SmartProcessEntityId = $_ENV['BITRIX_24_SMART_PROCESS_ENTITY_ID'];
$bitrix24SmartProcessCadNumFieldId = $_ENV['BITRIX_24_SMART_PROCESS_CAD_NUM_FIELD_ID'];
$bitrix24SmartProcessAddressFieldId = $_ENV['BITRIX_24_SMART_PROCESS_ADDRESS_FIELD_ID'];
$bitrix24SmartProcessAreaFieldId = $_ENV['BITRIX_24_SMART_PROCESS_AREA_FIELD_ID'];
$bitrix24SmartProcessObjTypeFieldId = $_ENV['BITRIX_24_SMART_PROCESS_OBJ_TYPE_FIELD_ID'];
$bitrix24LeadSmartProcessConnectionFieldId = $_ENV['BITRIX_24_LEAD_SMART_PROCESS_CONNECTION_FIELD_ID'];
$bitrix24LeadRealEstateObjectsFieldId = $_ENV['BITRIX_24_LEAD_REAL_ESTATE_OBJECTS'];
$bitrix24BalanceNotificationUserId = $_ENV['BITRIX_24_BALANCE_NOTIFICATION_USER_ID'];
$isDebug = (bool) $_ENV['DEBUG'];

$logDirPath = __DIR__ . '/log';
$log = new Logger('main');
$log->pushHandler(new StreamHandler($logDirPath . '/main.log', Logger::WARNING));
if ($isDebug) {
    $log->pushHandler(new StreamHandler($logDirPath . '/debug.log', Logger::DEBUG));
}