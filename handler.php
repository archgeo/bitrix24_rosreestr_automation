<?php

require __DIR__ . '/bootstrap.php';

use Bitrix24Api\Client as Bitrix24ApiClient;
use ReestrApi\Client as ReestrApiClient;

$log->info('Input request to /handler.php with folowing query params: ' . var_export($_GET, true));

if (empty($_GET['lead_id'])) {
    $log->error("Empty 'lead_id' query param.");
    exit(1);
}

if (empty($_GET['cad_num'])) {
    $log->error("Empty 'cad_num' query param.");
    exit(1);
}

$bitrix24LeadId = $_GET['lead_id'];
$cadNum = $_GET['cad_num'];

$reestrClient = new ReestrApiClient($reestrApiToken, $log);
$bitrix24Client = new Bitrix24ApiClient($bitrix24WebhookUrl, $log);

try {
    $realEstateObjects = $reestrClient->getRealEstateObjectByCadastr($cadNum);
} catch (\Throwable $th) {
    $errorMessage = 'Error while getting real estate object by cad num via ReestrApi. ' . $th->getMessage();
    $log->error($errorMessage);
    
    $b24LeadCommentTemplate = "🚨 Возникла ошибка при получении данных по объекту по кадастровому номеру %s. Ошибка - %s";
    $b24LeadComment = sprintf($b24LeadCommentTemplate, $cadNum, $th->getMessage());
    try {
        createLeadComment($bitrix24Client, $bitrix24LeadId, $b24LeadComment);
    } catch (\Throwable $th) {
        $errorMessage = 'Error while sending comment to Bitrix24 lead. ' . $th->getMessage();
        $log->error($errorMessage);
    }
    exit(1);
}

$log->info('Real estate objects filtered by cad num: ' . var_export($realEstateObjects, true));

if (empty($realEstateObjects)) {
    $log->warning('Empty real estate objects for cad num - ' . $cadNum);

    $b24LeadComment = sprintf("🚨 Данные объекта по кадастровому номеру - %s не найдены.", $cadNum);
    try {
        createLeadComment($bitrix24Client, $bitrix24LeadId, $b24LeadComment);
    } catch (\Throwable $th) {
        $errorMessage = 'Error while sending comment to Bitrix24 lead. ' . $th->getMessage();
        $log->error($errorMessage);
    }
    exit(1);
}

$realEstateObjectAddress = null;
$realEstateObjectArea = null;
$realEstateObjectType = null;

foreach ($realEstateObjects as $object) {
    if (isset($object['address']) && isset($object['area'])) {
        $realEstateObjectArea = $object['area'];
        $realEstateObjectAddress = $object['address'];
        $realEstateObjectType = $object['obj_type'];
    }
}

if (is_null($realEstateObjectAddress) || is_null($realEstateObjectArea) || is_null($realEstateObjectType)) {
    $log->error("Empty real estate object's address, area or type");

    $b24LeadComment = "🚨 Не удалось получить данные об адресу, площади или типу объекта по кадастровому номеру - " . $cadNum;
    try {
        createLeadComment($bitrix24Client, $bitrix24LeadId, $b24LeadComment);
    } catch (\Throwable $th) {
        $errorMessage = 'Error while sending comment to Bitrix24 lead. ' . $th->getMessage();
        $log->error($errorMessage);
    }
    exit(1);
}

$filter = [
    $bitrix24SmartProcessCadNumFieldId => $cadNum
];
$select = ['id'];
try {
    $smartProcessItems = getSmartProcessItemList(
        $bitrix24Client, 
        $bitrix24SmartProcessEntityId, 
        $filter,
        $select);
} catch (\Throwable $th) {
    $errorMessage = 'Error while filtering Bitrix24 smart process items by cad num. ' . $th->getMessage();
    $log->error($errorMessage);
    exit(1);
}

$log->info('Smart process items filtered by cad num: ' . var_export($smartProcessItems, true));

$smartProcessItemId = null;
if (count($smartProcessItems) != 0) {
    $smartProcessItemId = $smartProcessItems[0]['id'];
}

$smartProcessItemFields = [
    'title' => $realEstateObjectAddress,
    $bitrix24SmartProcessCadNumFieldId => $cadNum,
    $bitrix24SmartProcessAddressFieldId => $realEstateObjectAddress,
    $bitrix24SmartProcessAreaFieldId => $realEstateObjectArea,
    $bitrix24SmartProcessObjTypeFieldId => $realEstateObjectType,
];

if (is_null($smartProcessItemId)) {
    try {
        $smartProcessItem = addSmartProcessItem(
            $bitrix24Client, 
            $bitrix24SmartProcessEntityId, 
            $smartProcessItemFields);
    } catch (\Throwable $th) {
        $errorMessage = 'Error while adding new Bitrix24 smart process item. ' . $th->getMessage();
        $log->error($errorMessage);
        exit(1);
    }

    $log->info('New smart process item created: ' . var_export($smartProcessItem, true));
    $smartProcessItemId = $smartProcessItem['id'];
}
else {
    try {
        $smartProcessItem = updateSmartProcessItem(
            $bitrix24Client, 
            $bitrix24SmartProcessEntityId,
            $smartProcessItemId,
            $smartProcessItemFields);
        
        $log->info('Smart process item updated: ' . var_export($smartProcessItem, true));
    } catch (\Throwable $th) {
        $errorMessage = 'Error while updating Bitrix24 smart process item. ' . $th->getMessage();
        $log->error($errorMessage);
        exit(1);
    }
}

try {
    $lead = getLead(
        $bitrix24Client, 
        $bitrix24LeadId);
} catch (\Throwable $th) {
    $errorMessage = 'Error while getting Bitrix24 lead. ' . $th->getMessage();
    $log->error($errorMessage);
    exit(1);
}

$log->info('Current lead data: ' . var_export($lead, true));

$leadSmartProcessConnectionFieldVal = [];
if (isset($lead[$bitrix24LeadSmartProcessConnectionFieldId])) {
    if ($lead[$bitrix24LeadSmartProcessConnectionFieldId] !== false) {
        $leadSmartProcessConnectionFieldVal = $lead[$bitrix24LeadSmartProcessConnectionFieldId];
    }
}

if (!in_array($smartProcessItemId, $leadSmartProcessConnectionFieldVal)) {
    $leadSmartProcessConnectionFieldVal[] = $smartProcessItemId;
}

$leadRealEstateObjectsFieldVal = '';
if (isset($lead[$bitrix24LeadRealEstateObjectsFieldId])) {
    $leadRealEstateObjectsFieldVal = $lead[$bitrix24LeadRealEstateObjectsFieldId];
}

$leadRealEstateObjectsFieldVal .= sprintf("\n\nКадастровый номер: %s\nАдрес: %s\nПлощадь: %s\nТип: %s", 
    $cadNum,
    $realEstateObjectAddress,
    $realEstateObjectArea,
    $realEstateObjectType
);

$leadFields = [
    $bitrix24LeadSmartProcessConnectionFieldId => $leadSmartProcessConnectionFieldVal,
    $bitrix24LeadRealEstateObjectsFieldId => $leadRealEstateObjectsFieldVal
];

$log->info('Lead\'s fields to update: ' . var_export($leadFields, true));

try {
    $result = updateLead(
        $bitrix24Client, 
        $bitrix24LeadId,
        $leadFields);
} catch (\Throwable $th) {
    $errorMessage = 'Error while getting Bitrix24 lead. ' . $th->getMessage();
    $log->error($errorMessage);
    exit(1);
}

$log->info('Lead updated: ' . var_export($result, true));
