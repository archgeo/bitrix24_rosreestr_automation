<?php

require __DIR__ . '/bootstrap.php';

use Bitrix24Api\Client as Bitrix24ApiClient;
use ReestrApi\Client as ReestrApiClient;

if (php_sapi_name() !== 'cli') {
    exit(1);
}

$reestrClient = new ReestrApiClient($reestrApiToken, $log);

try {
    $balance = $reestrClient->getBalance();
} catch (\Throwable $th) {
    $errorMessage = 'Error while getting ReestrApi balance. ' . $th->getMessage();
    $log->error($errorMessage);
    print($errorMessage . "\n");
    exit(1);
}

if ($balance < 50) {
    $bitrix24Client = new Bitrix24ApiClient($bitrix24WebhookUrl, $log);
    $messageTemplate = "Баланс средств на счете в сервисе Реестр API составляет %.2f руб.\n";
    $messageTemplate .= "Когда средства закончатся, у вас не будет возможность получать данные по кадастровым номерам.";
    $notificationMessage = sprintf($messageTemplate, $balance);

    try {
        $result = sendSystemNotification(
            $bitrix24Client, 
            $bitrix24BalanceNotificationUserId, 
            $notificationMessage);
    } catch (\Throwable $th) {
        $errorMessage = 'Error while sending Bitrix24 notification about low ReestrApi balance. ' . $th->getMessage();
        $log->error($errorMessage);
        print($errorMessage . "\n");
        exit(1);
    }

    if ($result === false) {
        $errorMessage = "Error while sending Bitrix24 notification about low ReestrApi balance";
        $log->error($errorMessage);
        print($errorMessage . "\n");
        exit(1);
    }
}

