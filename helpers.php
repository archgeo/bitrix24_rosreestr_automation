<?php

function getSmartProcessTypes($b24Client) {
    $smartProcessTypes = [];

    $apiMethod = 'crm.type.list';
    $responseBody = $b24Client->makeRequest('GET', $apiMethod);

    if (isset($responseBody['result']) && isset($responseBody['result']['types'])) {
        $smartProcessTypes = $responseBody['result']['types'];
    }

    return $smartProcessTypes;
}

function getSmartProcessFields($b24Client, $smartProcessEntityTypeId) {
    $smartProcessFields = [];

    $apiMethod = 'crm.item.fields';
    $options = [
        'query' => [
            'entityTypeId' => $smartProcessEntityTypeId
        ]
    ];
    $responseBody = $b24Client->makeRequest('GET', $apiMethod, $options);

    if (isset($responseBody['result']) && isset($responseBody['result']['fields'])) {
        $smartProcessFields = $responseBody['result']['fields'];
    }

    return $smartProcessFields;
}

function getLeadFields($b24Client) {
    $leadFields = [];

    $apiMethod = 'crm.lead.fields';
    $responseBody = $b24Client->makeRequest('GET', $apiMethod);

    if (isset($responseBody['result'])) {
        $leadFields = $responseBody['result'];
    }

    return $leadFields;
}

function getSmartProcessItemList($b24Client, $smartProcessEntityTypeId, $filter, $select) {
    $itemList = [];

    $apiMethod = 'crm.item.list';
    $options = [
        'query' => [
            'entityTypeId' => $smartProcessEntityTypeId,
            'filter' => $filter,
            'select' => $select
        ]
    ];
    $responseBody = $b24Client->makeRequest('GET', $apiMethod, $options);

    if (isset($responseBody['result']) && isset($responseBody['result']['items'])) {
        $itemList = $responseBody['result']['items'];
    }

    return $itemList;
}

function addSmartProcessItem($b24Client, $smartProcessEntityTypeId, $fields) {
    $item = [];

    $apiMethod = 'crm.item.add';
    $options = [
        'form_params' => [
            'entityTypeId' => $smartProcessEntityTypeId,
            'fields' => $fields
        ]
    ];
    $responseBody = $b24Client->makeRequest('POST', $apiMethod, $options);
    
    if (isset($responseBody['result']) && isset($responseBody['result']['item'])) {
        $item = $responseBody['result']['item'];
    }

    return $item;
}

function updateSmartProcessItem($b24Client, $smartProcessEntityTypeId, $itemId, $fields) {
    $item = [];

    $apiMethod = 'crm.item.update';
    $options = [
        'form_params' => [
            'entityTypeId' => $smartProcessEntityTypeId,
            'id' => $itemId,
            'fields' => $fields
        ]
    ];
    $responseBody = $b24Client->makeRequest('POST', $apiMethod, $options);
    
    if (isset($responseBody['result']) && isset($responseBody['result']['item'])) {
        $item = $responseBody['result']['item'];
    }

    return $item;
}

function createCrmEntityComment($b24Client, $entityId, $entityType, $commentBody) {
    $commentId = null;

    $apiMethod = 'crm.timeline.comment.add';
    $options = [
        'form_params' => [
            'fields' => [
                'ENTITY_ID' => $entityId,
                'ENTITY_TYPE' => $entityType,
                'COMMENT' => $commentBody
            ]
        ]
    ];
    $responseBody = $b24Client->makeRequest('POST', $apiMethod, $options);
    
    if (isset($responseBody['result'])) {
        $commentId = $responseBody['result'];
    }

    return $commentId;
}

function createLeadComment($b24Client, $leadId, $commentBody) {
    return createCrmEntityComment($b24Client, $leadId, 'lead', $commentBody);
}

function getLead($b24Client, $leadId) {
    $lead = [];

    $apiMethod = 'crm.lead.get';
    $options = [
        'query' => [
            'id' => $leadId
        ]
    ];
    $responseBody = $b24Client->makeRequest('GET', $apiMethod, $options);

    if (isset($responseBody['result'])) {
        $lead = $responseBody['result'];
    }

    return $lead;
}

function updateLead($b24Client, $leadId, $fields) {
    $result = false;

    $apiMethod = 'crm.lead.update';
    $options = [
        'form_params' => [
            'id' => $leadId,
            'fields' => $fields
        ]
    ];
    $responseBody = $b24Client->makeRequest('POST', $apiMethod, $options);

    if (isset($responseBody['result'])) {
        $result = (bool) $responseBody['result'];
    }

    return $result;
}

/*
 * return:
 * - notification id if notification sended
 * - false if notifications did'n send
 * 
 * throw Bitrix24ApiException
 */ 
function sendSystemNotification($b24Client, $userId, $messageBody) {
    $notificationId = false;

    $apiMethod = 'im.notify.system.add';
    $options = [
        'form_params' => [
            'USER_ID' => $userId,
            'MESSAGE' => $messageBody
        ]
    ];
    $responseBody = $b24Client->makeRequest('POST', $apiMethod, $options);

    if (isset($responseBody['result'])) {
        $notificationId = $responseBody['result'];
    }

    return $notificationId;
}
