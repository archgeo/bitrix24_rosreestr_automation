<?php

require __DIR__ . '/bootstrap.php';

use Bitrix24Api\Client as Bitrix24ApiClient;

if (php_sapi_name() !== 'cli') {
    exit(1);
}

$bitrix24Client = new Bitrix24ApiClient($bitrix24WebhookUrl, $log);

print("Smart Process Types\n------------------------\n");
$smartProcessTypes = [];

try {
    $smartProcessTypes = getSmartProcessTypes($bitrix24Client);
} catch (\Throwable $th) {
    printf("Ошибка при получении списка типов смарт-процессов: %s \n",
            $th->getMessage());
}

if (count($smartProcessTypes)) {
    foreach ($smartProcessTypes as $type) {
        printf("* %s (id - %s; entityTypeId - %s)\n", 
            $type['title'], 
            $type['id'], 
            $type['entityTypeId']);
        
        print("  Fields:\n");
        $smartProcessFields = [];

        try {
            $smartProcessFields = getSmartProcessFields($bitrix24Client, $type['entityTypeId']);
        } catch (\Throwable $th) {
            printf("  Ошибка при получении списка полей смарт-процессов: %s \n",
                    $th->getMessage());
        }

        if (count($smartProcessFields)) {
            foreach ($smartProcessFields as $fieldId => $field) {
                printf("  - %s (id - %s; type - %s)\n", 
                    $field['title'],
                    $fieldId,
                    $field['type']);
            }
        }
    }
}
print("\n");

print("Lead Fields\n------------------------\n");
$leadFields = [];

try {
    $leadFields = getLeadFields($bitrix24Client);
} catch (\Throwable $th) {
    printf("Ошибка при получении списка полей лида: %s \n",
            $th->getMessage());
}

if (count($leadFields)) {
    foreach ($leadFields as $fieldId => $field) {
        printf("* %s (id - %s; type - %s)\n", 
            isset($field['formLabel']) ? $field['formLabel'] : $field['title'], 
            $fieldId,
            $field['type']);        
    }
}
