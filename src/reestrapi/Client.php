<?php

namespace ReestrApi;

use GuzzleHttp\Client as GuzzleHttpClient;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\MessageFormatter;
use GuzzleHttp\Middleware;
use GuzzleHttp\Exception\TransferException;

/*
 * Клиент для работы с сервисом Реестр API
 * https://reestr-api.ru
 */
class Client {
    private $httpClient;
    private $apiToken;

    public function __construct($apiToken, $logger = null, $baseUrl = 'https://reestr-api.ru/v1/') {
        $this->apiToken = $apiToken;
        
        $stack = HandlerStack::create();
        if (!is_null($logger)) {
            $stack->push(Middleware::log($logger, new MessageFormatter(MessageFormatter::DEBUG)));
        }

        $this->httpClient = new GuzzleHttpClient([
            'base_uri' => $baseUrl,
            'http_errors' => true,
            'timeout'  => 5.0,
            'handler' => $stack
        ]);
    }

    public function getRealEstateObjectByCadastr($cadNum, $isFull = false) {
        if ($isFull) {
            $apiMethod = 'search/cadastrFull';
        }
        else {
            $apiMethod = 'search/cadastr';
        }
        
        $options = [
            'form_params' => [
                'cad_num' => $cadNum
            ]
        ];

        $response = $this->makeRequest('POST', $apiMethod, $options);
        $responseBody = $this->decodeResponseBody($response);
        $this->checkApiLevelErrors($responseBody);

        return $responseBody['list'];
    }

    public function getBalance() {
        $apiMethod = 'account/balance';
        $response = $this->makeRequest('POST', $apiMethod);
        $responseBody = $this->decodeResponseBody($response);
        $this->checkApiLevelErrors($responseBody);

        return (float) $responseBody['sum'];
    }

    private function makeRequest($httpMethod, $apiMethod, $options = []) {
        if (!isset($options['query'])) {
            $options['query'] = [];
        }
        $options['query']['auth_token'] = $this->apiToken;

        try {
            $response = $this->httpClient->request($httpMethod, $apiMethod, $options);
        } catch (TransferException $e) {
            throw new ReestrApiException($e->getMessage(), 1);
        }

        return $response;
    }

    private function decodeResponseBody($response) {
        $body = (string) $response->getBody();
        $decodedBody = json_decode($body, true);

        if ($decodedBody === null && json_last_error() !== JSON_ERROR_NONE) {
            $errorMsg = 'Response body json decoding error: ' . json_last_error_msg();
            $errorMsg .= ' Response body: ' . $body;
            throw new ReestrApiException($errorMsg, 1);
        }

        return $decodedBody;
    }

    private function checkApiLevelErrors($responseBody) {
        if (!isset($responseBody['query'])) {
            $errorMsg = 'Response error: `query` key not set. Response body: ' . json_encode($responseBody);
            throw new ReestrApiException($errorMsg, 1);
        }

        if ($responseBody['query'] !== 'success') {
            $errorMsg = 'Response error: `query` status not success. Response body: ' . json_encode($responseBody);
            throw new ReestrApiException($errorMsg, 1);
        }
    }
}