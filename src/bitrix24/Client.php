<?php

namespace Bitrix24Api;

use GuzzleHttp\Client as GuzzleHttpClient;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\MessageFormatter;
use GuzzleHttp\Middleware;
use GuzzleHttp\Exception\TransferException;

/*
 * Клиент для работы с Bitrix24 REST API
 * с использованием входящего вебхука
 * https://dev.1c-bitrix.ru/rest_help/
 */
class Client {
    private $webhookUrl;
    private $httpClient;

    public function __construct($webhookUrl, $logger = null) {
        $this->webhookUrl = $webhookUrl;
        
        $stack = HandlerStack::create();
        if (!is_null($logger)) {
            $stack->push(Middleware::log($logger, new MessageFormatter(MessageFormatter::DEBUG)));
        }

        $this->httpClient = new GuzzleHttpClient([
            'http_errors' => true,
            'timeout'  => 5.0,
            'handler' => $stack
        ]);
    }

    public function makeRequest($httpMethod, $apiMethod, $options = []) {
        try {
            $url = $this->webhookUrl . $apiMethod . '.json';
            $response = $this->httpClient->request(
                $httpMethod, 
                $url,
                $options);
        } catch (TransferException $e) {
            throw new Bitrix24ApiException($e->getMessage(), 1);
        }

        $responseBody = $this->decodeResponseBody($response);
        $this->checkApiLevelErrors($responseBody);

        return $responseBody;
    }

    private function decodeResponseBody($response) {
        $body = (string) $response->getBody();
        $decodedBody = json_decode($body, true);

        if ($decodedBody === null && json_last_error() !== JSON_ERROR_NONE) {
            $errorMsg = 'Response body json decoding error: ' . json_last_error_msg();
            $errorMsg .= ' Response body: ' . $body;
            throw new Bitrix24ApiException($errorMsg, 1);
        }

        return $decodedBody;
    }

    private function checkApiLevelErrors($responseBody) {
        if (isset($responseBody['error'])) {
            $errorMsg = 'Response error. Response body: ' . json_encode($responseBody);
            throw new Bitrix24ApiException($errorMsg, 1);
        }
    }
}